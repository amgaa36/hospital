//
//  AlertBuilder.swift
//  mado
//
//  Created by Enkh-Amgalan on 10/21/16.
//  Copyright © 2016 Enkh-Amgalan. All rights reserved.
//

import UIKit
import SwiftMessages

class AlertBuilder: NSObject {

    static let sharedInstance = AlertBuilder()
    
    func showError(message text: String, title: String){
        
        let error = MessageView.viewFromNib(layout: .cardView)
        error.configureTheme(.error)
        error.configureDropShadow()
        error.configureContent(title: title, body: text)
        error.button?.isHidden = true
        var errorConfig = SwiftMessages.Config()
        errorConfig.presentationStyle = .top
        errorConfig.presentationContext = .window(windowLevel: UIWindowLevelNormal)
        
        SwiftMessages.show(config: errorConfig, view: error)

    }
    
    func showSuccess(message text: String, title: String, duration: Int = 3){
        
        let error = MessageView.viewFromNib(layout: .cardView)
        error.configureTheme(.success)
        error.configureDropShadow()
        error.configureContent(title: title, body: text)
        error.button?.isHidden = true
        var errorConfig = SwiftMessages.Config()
        errorConfig.presentationStyle = .top
        errorConfig.interactiveHide = true
        errorConfig.duration = SwiftMessages.Duration.seconds(seconds: TimeInterval(duration))
        errorConfig.presentationContext = .window(windowLevel: UIWindowLevelNormal)
        
        SwiftMessages.show(config: errorConfig, view: error)
    }

    
}
