//
//  APIProvider.swift
//  hospital
//
//  Created by Ankhbayar TS on 8/25/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Disk

typealias SuccessClosure = (JSON)->Void
typealias ErrorClosure = (String)->Void

class APIProvider: NSObject {
    
    func getSpecials(success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.Domain.domain + "medical/special"
        self.sendRequest(url: url, success: success, error: error)
    }
    
    
    func searchHospital(page: Int = 1, name: String = "", success: SuccessClosure?, error: ErrorClosure?){
        let labIds = HospitalDataManager.shared.selectedLabs
        var labs = ""
        var typeId = ""
        if let type = HospitalDataManager.shared.type {
            typeId = "\(type.id)"
        }
        var url = ""
        if labIds.count > 0 {
            for lab in labIds {
                labs  = labs + "&labIds[]=\(lab)"
            }
            url = Constants.Domain.domain + "medical/list/\(page)/\(typeId)?name=\(name)\(labs)"
        }else{
            url = Constants.URL.hospitalList + String(page) + "/" + typeId
        }
        self.sendRequest(url: url, success: success, error: error)
    }
    
    func serviceList(success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.Domain.domain + "medical/lab-type"
        self.sendRequest(url: url, success: success, error: error)
    }
    
    func getHospitalList(page: Int = 1, type: HospitalType, success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.URL.hospitalList + String(page) + "/" + type.id
        self.sendRequest(url: url, success: success, error: error)
    }
    
    func getDetail(hospitalId: Int, success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.URL.hospitalDetail + String(hospitalId)
        self.sendRequest(url: url, success: success, error: error)
    }
    
    func getTypes(success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.URL.typeList
        self.sendRequest(url: url, success: success, error: error)
    }
    
    func getSuvilagchList(type: Int, position: Int, success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.URL.nurseList
        let params = ["position": position, "services": type]
        sendRequest(url: url, params: params, success: success, error: error)
    }
    
    
    func getSuvilagchType(success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.URL.nurseType
        self.sendRequest(url: url, success: success, error: error)
    }
    
    func getSuvilagchPosition(success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.URL.nursePosition
        self.sendRequest(url: url, success: success, error: error)
    }
    
    func sendSuvilagchLog(positionId: Int, nurse: Nurse, success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.Domain.domain + "nurse/log"
        let params: [String: Any] = ["nurseid": nurse.nurseId, "nursename": nurse.nurseName, "price": nurse.servicePrice, "position": positionId]
        sendRequest(url: url, method: .post, params: params, success: success, error: error)
    }
    
    func sendFeedback(nurse: Nurse, content: String, success: SuccessClosure?, error: ErrorClosure?){
        let url = Constants.Domain.webDomain + "api/nurse/feedback"
        let params: [String: Any] = ["id": nurse.nurseId, "content": content]
        self.sendRequest(url: url, method: .post, params: params, success: success, error: error)
    }
    
    func getDiagnoseList(type: Int, success: SuccessClosure?, error: ErrorClosure?) {
        let url = Constants.URL.diagnoseTestList + "\(type)"
        self.sendRequest(url: url, success: success, error: error)
    }
    
    func getDiagnoseDetail(diagnose: Int,success: SuccessClosure?, error: ErrorClosure?) {
        let url = Constants.URL.diagnoseTestDetail + "\(diagnose)"
        self.sendRequest(url: url, success: success, error: error)
    }
    
    
    func sendRequest(url: String, method:HTTPMethod = .get , params: [String: Any]? = nil, success: SuccessClosure?, error: ErrorClosure? ){
        
//        print("sending request to : \(url) \(params)")
        Alamofire.request(url, method: method, parameters: params, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
//                print("Validation Successful")
                let json = JSON(data: response.data!)
                print(json)
                success!(json)
                do {
                    try Disk.save(response.data!, to: .caches, as: url)
                }catch {
                    print("can not save data to local disk for cache")
                }
            case .failure(let error):
                do {
                    
                    let cached = try Disk.retrieve(url, from: .caches, as: Data.self)
                    let json = JSON(data: cached)
                    success!(json)
                    
                }catch{
                    print("can not retrieve data from local cache")
                }
                
                print(error)
            }
        }
        
    }

}
