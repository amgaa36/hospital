//
//  TypeCell.swift
//  hospital
//
//  Created by Ankhbayar TS on 9/2/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class TypeCell: UICollectionViewCell {
    
    let imageView = UIImageView()
    let nameLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView.contentMode = .scaleAspectFit
        nameLabel.textAlignment = .center
        nameLabel.textColor = .lightGray
        nameLabel.font = UIFont.systemFont(ofSize: 13)
        
        backgroundColor = .white
        contentView.addSubview(imageView)
        contentView.addSubview(nameLabel)
        setConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setConstraint(){
        imageView.snp.makeConstraints { (const) in
            const.top.equalTo(20)
            const.left.right.equalToSuperview()
            const.height.equalTo(contentView.snp.height).multipliedBy(0.5).offset(-20)
        }
        nameLabel.snp.makeConstraints { (const) in
            const.top.equalTo(imageView.snp.bottom)
            const.bottom.left.right.equalToSuperview()
        }
    }
    
    func setData(data: HospitalType){
        
        if let url = URL(string: data.photo){
            print(data.photo)
            imageView.kf.setImage(with: url)
        }
        nameLabel.text = data.name

        
    }
    
}
