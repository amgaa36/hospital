//
//  MCGalleryDetailView.swift
//  MyanCashAgent
//
//  Created by moco on 11/1/16.
//  Copyright © 2016 Mongol Content. All rights reserved.
//

import UIKit
import Kingfisher


/// View for a banner that is shown in slider.
class MCGalleryDetailView: UIView {
    internal var urlPath: String = ""
    internal let imageView = UIImageView.init()
    internal let spinner = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
    var tapHandler: ((MCGalleryDetailView)->Void)?
    
    init(frame: CGRect, url: String) {
        super.init(frame: frame)
        
        self.urlPath = url
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.clipsToBounds = true
        
        self.spinner.hidesWhenStopped = true
        self.spinner.stopAnimating()
        addSubview(self.imageView)
        addSubview(self.spinner)
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.addGestureRecognizer(tgr)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func tapped(){
        if let handler = self.tapHandler {
            handler(self)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView.frame = CGRect(origin: CGPoint.zero, size: frame.size)
        self.spinner.center = imageView.center
    }
    
    /// Retrieving image from the URL.
    func loadImage() {
        let url = URL(string: self.urlPath)
        imageView.kf.setImage(with: url, placeholder: UIImage(named: "banner.jpg"), options: nil, progressBlock: nil, completionHandler: nil)
    }

}
