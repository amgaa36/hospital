//
//  HomeMenuButton.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/18/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SnapKit

class HomeMenuButton: UIButton {

    let nameLabel = UILabel()
    let iconView = UIImageView()
    
    convenience init(name: String, icon: UIImage) {
        self.init(frame: .zero)
        
        iconView.contentMode = .scaleAspectFit
        iconView.image = icon
        
        nameLabel.textAlignment = .center
        nameLabel.text = name.uppercased()
        nameLabel.font = UIFont(name: "Roboto", size: 16)
        nameLabel.textColor = .gray
        
        layer.cornerRadius = 2
        layer.borderColor = UIColor(white: 233/255, alpha: 1).cgColor
        layer.borderWidth = 1
        
        addSubview(nameLabel)
        addSubview(iconView)
        
        iconView.snp.makeConstraints { (const) in
            const.left.right.equalToSuperview()
            const.height.equalToSuperview().multipliedBy(0.4)
            const.bottom.equalTo(self.snp.centerY).offset(10)
        }
        
        nameLabel.snp.makeConstraints { (const) in
            const.left.right.equalToSuperview()
            const.top.equalTo(iconView.snp.bottom)
            const.bottom.equalToSuperview()
        }
    }
    
    override init(frame: CGRect){
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
