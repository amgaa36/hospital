//
//  EmchCell.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/3/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON

class EmchCell: UICollectionViewCell {
    let nameLabel = UILabel()
    let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        nameLabel.font = UIFont.systemFont(ofSize: 12)
        nameLabel.textColor = UIColor(red: 0.1, green: 0.1, blue: 0.8, alpha: 1)
        nameLabel.textAlignment = .center
        nameLabel.numberOfLines = 2
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        contentView.addSubview(nameLabel)
        contentView.addSubview(imageView)
        
        imageView.snp.makeConstraints { (const) in
            const.left.right.equalTo(nameLabel)
            const.top.equalTo(10)
            const.bottom.equalTo(nameLabel.snp.top).offset(-5)
        }
        nameLabel.snp.makeConstraints { (const) in
            const.left.equalTo(10)
            const.right.equalTo(-10)
            const.bottom.lessThanOrEqualTo(-10)
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setData(data: JSON){
        nameLabel.text = data["name"].stringValue
        imageView.kf.setImage(with: URL(string: Constants.Domain.resourceDomain + data["photo"].stringValue))
    }
    
}
