//
//  TasagCell.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/3/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON

class TasagCell: UICollectionViewCell {
    let nameLabel = UILabel()
    let priceLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont.systemFont(ofSize: 13)
        nameLabel.textColor = UIColor(red: 0.1, green: 0.1, blue: 0.8, alpha: 1)
        nameLabel.textAlignment = .center
        
        priceLabel.numberOfLines = 0
        priceLabel.font = UIFont.systemFont(ofSize: 11)
        
        contentView.addSubview(nameLabel)
        contentView.addSubview(priceLabel)
        
        nameLabel.snp.makeConstraints { (const) in
            const.left.equalTo(10)
            const.right.equalTo(-10)
            const.top.equalTo(10)
        }
        priceLabel.snp.makeConstraints { (const) in
            const.left.right.equalTo(nameLabel)
            const.top.equalTo(nameLabel.snp.bottom).offset(5)
            const.bottom.lessThanOrEqualTo(-10)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setData(data: JSON){
        nameLabel.text = data["oroo_ner"].stringValue
        let ontslog = data["oroo_ontslog"].arrayValue
        var text = "Өрөөний тоо: \(data["oroo_too"].stringValue)\nҮнэ: \(data["oroo_une"].stringValue)\n"
        for onts in ontslog {
            text += onts.stringValue + "\n"
        }
        priceLabel.text = text
    }
}
