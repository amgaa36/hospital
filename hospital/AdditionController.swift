//
//  AdditionController.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/3/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON

enum AdditionalType {
    case turul
    case emch
    case tasag
}

class AdditionController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var type: AdditionalType = .turul
    var json: JSON!
    var collection: UICollectionView!
    var jsonArray = [JSON]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch type {
        case .turul:
            jsonArray = json["medicalLabType"].arrayValue
        case .emch:
            jsonArray = json["medicalDoctor"].arrayValue
        case .tasag:
            jsonArray = json["tasagInfo"].arrayValue
        }
        
        view.backgroundColor = .white
        let collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.scrollDirection = .horizontal
        collection = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collection.register(TurulCell.self, forCellWithReuseIdentifier: "turulCell")
        collection.register(EmchCell.self, forCellWithReuseIdentifier: "emchCell")
        collection.register(TasagCell.self, forCellWithReuseIdentifier: "tasagCell")
        collection.dataSource = self
        collection.delegate = self
        collection.backgroundColor = UIColor(white: 0.95, alpha: 1)
        view.addSubview(collection)
        collection.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return jsonArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize!
        switch type {
        case .turul:
            size = CGSize(width: 150, height: 150)
        case .tasag:
            size = CGSize(width: 200, height: 150)
        case .emch:
            size = CGSize(width: 100, height: 150)
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellJson = jsonArray[indexPath.row]
        switch type {
        case .turul:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "turulCell", for: indexPath) as! TurulCell
            cell.setData(data: cellJson)
            return cell
        case .emch:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "emchCell", for: indexPath) as! EmchCell
            cell.setData(data: cellJson)
            return cell
        case .tasag:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tasagCell", for: indexPath) as! TasagCell
            cell.setData(data: cellJson)
            return cell
        }
        
    }
    
}
