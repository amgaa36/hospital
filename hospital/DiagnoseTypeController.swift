//
//  DiagnoseTypeController.swift
//  hospital
//
//  Created by Ankhbayar TS on 1/30/18.
//  Copyright © 2018 Amgaa. All rights reserved.
//

import UIKit

class DiagnoseTypeController: HViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let distance = 40
        self.title = "Та доорх 4 төрлөөс сонгоно уу?"
        let matureButton = DiagnoseTypeView()
        matureButton.imageView.image = #imageLiteral(resourceName: "diagnose_mature")
        matureButton.nameLabel.text = "Насанд хүрсэн".uppercased()
        matureButton.tapHandler = {
            let listController = DiagnoseListController()
            listController.id = 1
            self.navigationController?.pushViewController(listController, animated: true)
        }
        
        let childButton = DiagnoseTypeView()
        childButton.imageView.image = #imageLiteral(resourceName: "diagnose_child")
        childButton.nameLabel.text = "Хүүхэд".uppercased()
        childButton.tapHandler = {
            let listController = DiagnoseListController()
            listController.id = 2
            self.navigationController?.pushViewController(listController, animated: true)
        }
        
        let femaleButton = DiagnoseTypeView()
        femaleButton.imageView.image = #imageLiteral(resourceName: "diagnose_female")
        femaleButton.nameLabel.text = "Эмэгтэй".uppercased()
        femaleButton.tapHandler = {
            let listController = DiagnoseListController()
            listController.id = 4
            self.navigationController?.pushViewController(listController, animated: true)
        }
        
        let maleButton = DiagnoseTypeView()
        maleButton.imageView.image = #imageLiteral(resourceName: "diagnose_male")
        maleButton.nameLabel.text = "Эрэгтэй".uppercased()
        maleButton.tapHandler = {
            let listController = DiagnoseListController()
            listController.id = 3
            self.navigationController?.pushViewController(listController, animated: true)
        }
        
        
        view.addSubview(matureButton)
        view.addSubview(childButton)
        view.addSubview(femaleButton)
        view.addSubview(maleButton)
        
        matureButton.snp.makeConstraints { (const) in
            const.right.equalTo(view.snp.centerX).offset(-distance/2)
            const.width.equalTo(100)
            const.height.equalTo(140)
            const.top.equalTo(80)
        }
        
        childButton.snp.makeConstraints { (const) in
            const.top.height.width.equalTo(matureButton)
            const.left.equalTo(matureButton.snp.right).offset(distance)
        }
        
        maleButton.snp.makeConstraints { (const) in
            const.left.width.height.equalTo(matureButton)
            const.top.equalTo(matureButton.snp.bottom).offset(distance)
        }
        
        femaleButton.snp.makeConstraints { (const) in
            const.top.height.width.equalTo(maleButton)
            const.left.equalTo(childButton)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
