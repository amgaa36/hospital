//
//  HomeViewController.swift
//  hospital
//
//  Created by Ankhbayar TS on 9/2/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import RealmSwift

class HomeViewController: HViewController {

    let galleryV = MCGalleryView()
    let collectionLayout = UICollectionViewFlowLayout()
    var typeCollection: UICollectionView!
    var typeArray = [HospitalType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        galleryV.isHome = true
        
        self.title = "Эмнэлэг"
        
        typeCollection = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        typeCollection.register(TypeCell.self, forCellWithReuseIdentifier: Constants.CellId.typeCellId)
        typeCollection.backgroundColor = UIColor.init(white: 0.95, alpha: 1)
        
        typeCollection.delegate = self
        typeCollection.dataSource = self
        
        let apiProvider = APIProvider()
        
        apiProvider.getSpecials(success: { (json) in
            let specials = json["medical"].arrayValue
            var imageUrls = [String]()
            if specials.count > 0 {
                for special in specials {
                    imageUrls.append(Constants.Domain.resourceDomain + special["photo"].stringValue)
                }
                self.galleryV.urls = imageUrls
                
                for index in 0..<imageUrls.count {
                    if let detail = self.galleryV.detailViews[index] as? HomeGalleryDetailView {
                        detail.nameLabel.text = specials[index]["name"].stringValue
                        detail.phoneButton.setTitle(specials[index]["phone"].stringValue, for: .normal)
                    }
                }
                self.galleryV.tappedImage = { index in
                    if specials.count > index {
                        let data = specials[index]
                        let detailController = DetailController()
                        detailController.hospitalId = data["id"].intValue
                        self.navigationController?.pushViewController(detailController, animated: true)
                    }
                }
            }
        }) { (errorString) in
            AlertBuilder.sharedInstance.showError(message: errorString, title: "Уучлаарай")
        }
        
        view.addSubview(galleryV)
        
        galleryV.snp.makeConstraints { const in
            const.top.leading.trailing.equalToSuperview()
            const.width.equalToSuperview()
            const.height.equalTo(180)
        }
        
        view.addSubview(typeCollection)
        typeCollection.snp.makeConstraints { (const) in
            const.top.equalTo(galleryV.snp.bottom)
            const.leading.trailing.bottom.equalTo(view)
        }
        
        
        apiProvider.getTypes(success: { (json) in
            let types = json["medicalType"].arrayValue
            if types.count > 0 {
                self.typeArray.removeAll()
                for typeJson in types {
                    let typeObject = HospitalType(json: typeJson)
                    self.typeArray.append(typeObject)
                }
                self.typeCollection.reloadData()
            }
        }) { (errorString) in
            print(errorString)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return typeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellId.typeCellId , for: indexPath) as! TypeCell
        let cellData = typeArray[indexPath.row]
        cell.setData(data: cellData)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width / 3 - 2, height: view.frame.width / 3 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(2, 2, 2, 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        HospitalDataManager.shared.type = typeArray[indexPath.row]
        HospitalDataManager.shared.currentPage = 0
        let listViewController = ViewController()
        self.navigationController?.pushViewController(listViewController, animated: true)
        
    }
    
}
