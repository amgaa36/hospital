//
//  DiagnoseTypeView.swift
//  hospital
//
//  Created by Ankhbayar TS on 1/30/18.
//  Copyright © 2018 Amgaa. All rights reserved.
//

import UIKit

class DiagnoseTypeView: UIView {

    let imageView = UIImageView()
    let nameLabel = UILabel()
    var tapHandler: (()->Void)!
    private let frameView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    init() {
        super.init(frame: .zero)
        setupView()
    }
    
    
    private func setupView(){
        
        nameLabel.textColor = Constants.Color.mainOrange
        nameLabel.font = UIFont(name: "Roboto", size: 16)
        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .center
        
        frameView.layer.borderColor = Constants.Color.mainGreen.cgColor
        frameView.layer.borderWidth = 1
        
        self.addSubview(imageView)
        self.addSubview(nameLabel)
        self.addSubview(frameView)
    
        imageView.snp.makeConstraints { (const) in
            const.edges.equalTo(frameView).inset(UIEdgeInsetsMake(20, 20, 20, 20))
        }
        
        frameView.snp.makeConstraints { (const) in
            const.width.top.centerX.equalToSuperview()
            const.height.equalTo(frameView.snp.width)
        }
        
        nameLabel.snp.makeConstraints { (const) in
            const.top.equalTo(frameView.snp.bottom).offset(5)
            const.left.equalTo(10)
            const.right.equalTo(-10)
        }
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(self.tapped))
        self.addGestureRecognizer(tgr)
        
    }
    
    @objc func tapped(){
        self.tapHandler()
    }
}
