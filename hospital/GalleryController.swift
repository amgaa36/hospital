//
//  GalleryController.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/1/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class GalleryController: HViewController, UIScrollViewDelegate {

    var imageUrl = ""
    private let scrollView = UIScrollView()
    private let imageView = UIImageView()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .black
        
        scrollView.addSubview(imageView)
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
        imageView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.delegate = self
        scrollView.maximumZoomScale = 2.0
        scrollView.minimumZoomScale = 1.0
        
        imageView.kf.setImage(with: URL(string: imageUrl))
        view.addSubview(scrollView)
        
        
    }
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }

}

class GalleryViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var controllers = [GalleryController]()
    var urls =  [String]() {
        didSet{
            for url in urls {
                let gallery = GalleryController()
                gallery.imageUrl = url
                self.controllers.append(gallery)
            }
            
        }
    }
    var startIndex: Int = 0
    private let closeButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViewControllers([controllers[startIndex]], direction: .forward, animated: false, completion: nil)
        self.delegate = self
        self.dataSource = self
        
        
        
        closeButton.setImage(#imageLiteral(resourceName: "ic_close_white"), for: .normal)
        closeButton.frame = CGRect(x: 20, y: 20, width: 40, height: 40)
        closeButton.addTarget(self, action: #selector(closeMe), for: .touchUpInside)
        view.addSubview(closeButton)
    }
    
    
    func closeMe(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let gallery = viewController as! GalleryController
        if let ind = controllers.index(of: gallery) {
            if ind < controllers.count - 1 {
                return controllers[ind + 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let gallery = viewController as! GalleryController
        if let ind = controllers.index(of: gallery) {
            if ind > 0 {
                return controllers[ind - 1]
            }
        }
        return nil
    }
    
    
}
