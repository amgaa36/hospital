//
//  HospitalCell.swift
//  hospital
//
//  Created by user04 on 8/29/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import Kingfisher

class HospitalCell: UITableViewCell {

    let callButton = UIButton()
    let contentBackView = UIView()
    let backImage = UIImageView()
    let titleLabel = UILabel()
    let descLabel = UILabel()
    var hospital: Hospital?
    var wifiImage = UIImageView()
    var visaImage = UIImageView()
    var carImage = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor(white: 0.95, alpha: 1)
        selectionStyle = .none
        contentBackView.backgroundColor = .white
        
        callButton.backgroundColor = Constants.Color.mainGreen
        callButton.setImage(#imageLiteral(resourceName: "ic_phone_white"), for: .normal)
        callButton.addTarget(self, action: #selector(self.callAction), for: .touchDown)
        backImage.contentMode = .scaleAspectFit
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        descLabel.numberOfLines = 3
        descLabel.font = UIFont.systemFont(ofSize: 13)
        
        contentView.addSubview(contentBackView)
        contentBackView.addSubview(backImage)
        contentBackView.addSubview(callButton)
        contentBackView.addSubview(titleLabel)
        contentBackView.addSubview(descLabel)
        
        wifiImage.image = #imageLiteral(resourceName: "ic_wifi").withRenderingMode(.alwaysTemplate)
        wifiImage.tintColor = UIColor.lightGray
        wifiImage.contentMode = .scaleAspectFit
        
        visaImage.image = #imageLiteral(resourceName: "ic_credit_card").withRenderingMode(.alwaysTemplate)
        visaImage.tintColor = UIColor.lightGray
        visaImage.contentMode = .scaleAspectFit
        
        carImage.image = #imageLiteral(resourceName: "ic_directions_car").withRenderingMode(.alwaysTemplate)
        carImage.tintColor = UIColor.lightGray
        carImage.contentMode = .scaleAspectFit
        
        contentBackView.addSubview(wifiImage)
        contentBackView.addSubview(visaImage)
        contentBackView.addSubview(carImage)
        
        
        self.setUpConstraint()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func setUpConstraint(){
        
        contentBackView.snp.makeConstraints { (const) in
            const.left.equalTo(contentView).offset(5)
            const.top.equalTo(2)
            const.right.equalTo(-5)
            const.bottom.equalTo(-2)
        }
        
        backImage.snp.makeConstraints { (const) in
            const.left.top.equalTo(5)
            const.bottom.equalTo(-5)
            const.width.equalTo(100)
        }
        
        titleLabel.snp.makeConstraints { (const) in
            const.top.equalTo(backImage)
            const.left.equalTo(backImage.snp.right).offset(10)
            const.right.equalTo(-5)
        }
        
        descLabel.snp.makeConstraints { (const) in
            const.right.left.equalTo(titleLabel)
            const.top.equalTo(titleLabel.snp.bottom).offset(5)
            const.bottom.lessThanOrEqualTo(-35)
            
        }
       
        carImage.snp.makeConstraints { (const) in
            const.right.equalTo(-10)
            const.bottom.equalTo(-5)
            const.height.equalTo(20)
            const.width.equalTo(30)
        }
        
        visaImage.snp.makeConstraints { (const) in
            const.top.bottom.width.equalTo(carImage)
            const.right.equalTo(carImage.snp.left).offset(-5)
        }
        
        wifiImage.snp.makeConstraints { (const) in
            const.top.bottom.width.equalTo(carImage)
            const.right.equalTo(visaImage.snp.left).offset(-5)
        }
        
        callButton.snp.makeConstraints { (const) in
            const.right.equalTo(wifiImage.snp.left).offset(-5)
            const.bottom.equalTo(0)
            const.height.equalTo(30)
            const.left.equalTo(descLabel.snp.left)
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func callAction(){
        if let hospital = self.hospital {
            if hospital.phone != "" {
                if let url = URL(string: "tel://\(hospital.phone)") {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    
    open func setData(data: Hospital){
        
        hospital = data
        
        if let url = URL(string: data.photo){
            print(data.photo)
            backImage.kf.setImage(with: url)
        }
        
        titleLabel.text = data.name
        descLabel.text = data.headline
        wifiImage.tintColor = data.isWifi ? Constants.Color.mainOrange : UIColor.lightGray
        visaImage.tintColor = data.isCard ? Constants.Color.mainOrange : UIColor.lightGray
        carImage.tintColor = data.isParking ? Constants.Color.mainOrange : UIColor.lightGray
    }

}
