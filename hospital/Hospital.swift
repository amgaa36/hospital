//
//  Hospital.swift
//  hospital
//
//  Created by user04 on 8/28/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

struct Hospital {
    
    var name: String
    var headline: String
    var phone: String
    var id: String
    var photo: String
    var isCard: Bool
    var isWifi: Bool
    var isTasag: Bool
    var isParking: Bool
    var coordinate: CLLocationCoordinate2D
    
    init(json: JSON) {
        name = json["name"].stringValue
        headline = json["headline"].stringValue
        phone = json["phone"].stringValue
        id = json["id"].stringValue
        photo = Constants.Domain.resourceDomain + json["photo"].stringValue
        isCard = json["isCard"].boolValue
        isWifi = json["isWifi"].boolValue
        isTasag = json["isTasag"].boolValue
        isParking = json["isParking"].boolValue
        let locString = json["longLat"].stringValue
        let arr = locString.components(separatedBy: ";")
        coordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        if arr.count == 2 {
            coordinate = CLLocationCoordinate2D(latitude: Double(arr[0])!, longitude: Double(arr[1])!)
        }
    }
    
}
