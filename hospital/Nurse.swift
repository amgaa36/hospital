//
//  Nurse.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/23/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Nurse {

    let nurseId: Int
    let nurseName: String
    let nurseNamtar: String
    let nursePhoto: String
    let nurseTurshlaga: String
    let nurseSurguuli: String
    let nurseMergeshil: String
    let servicePrice: Int
    let serviceId: Int
    let positionId: Int
    
    init(json: JSON){
        nurseId = json["nurseId"].intValue
        nurseName = json["nurseName"].stringValue
        nurseNamtar = json["nurseNamtar"].stringValue
        nursePhoto = Constants.Domain.webDomain + json["nursePhoto"].stringValue
        nurseTurshlaga = json["nurseTurshlaga"].stringValue
        nurseSurguuli = json["NurseSurguuli"].stringValue
        nurseMergeshil = json["NurseMergeshil"].stringValue
        servicePrice = json["ServicePrice"].intValue
        serviceId = json["ServiceId"].intValue
        positionId = json["PositionId"].intValue
    }
}
