//
//  DiagnoseQuestionController.swift
//  hospital
//
//  Created by Ankhbayar TS on 1/31/18.
//  Copyright © 2018 Amgaa. All rights reserved.
//

import UIKit



class DiagnoseQuestionController: DViewController {

    
    let stackView = UIStackView()
    var item: DiagnoseQuestionItem!
    var pageController: DiagnoseDetailPageController?
    let yesButton = UIButton()
    let noButton = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        self.title = "Та доорх асуултуудаас сонгоно уу"
        
        let questionImage = UIImageView()
        let questionLabel = UILabel()
        
        
        questionImage.image = #imageLiteral(resourceName: "question")
        questionImage.contentMode = .scaleAspectFit
        
        yesButton.setTitle("Тийм", for: .normal)
        yesButton.setTitleColor(.white, for: .normal)
        yesButton.backgroundColor = Constants.Color.mainOrange
        yesButton.titleLabel?.font = UIFont(name: "Roboto", size: 15)
        yesButton.addTarget(self, action: #selector(self.gotoNext(sender:)), for: .touchUpInside)
        
        noButton.setTitle("Үгүй", for: .normal)
        noButton.setTitleColor(.white, for: .normal)
        noButton.backgroundColor = Constants.Color.mainOrange
        noButton.titleLabel?.font = UIFont(name: "Roboto", size: 15)
        noButton.addTarget(self, action: #selector(self.gotoNext(sender:)), for: .touchUpInside)
        
        questionLabel.font = UIFont(name: "Roboto", size: 15)
        questionLabel.numberOfLines = 0
        questionLabel.textAlignment = .center
        
        let buttonBack = UIView()
        buttonBack.isUserInteractionEnabled = true
        buttonBack.addSubview(yesButton)
        buttonBack.addSubview(noButton)
        
        stackView.distribution = .fillProportionally
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 10
        
        stackView.addArrangedSubview(questionImage)
        stackView.addArrangedSubview(questionLabel)
        stackView.addArrangedSubview(buttonBack)
        
        view.addSubview(stackView)
        stackView.snp.makeConstraints { (const) in
            const.left.equalTo(30)
            const.right.equalTo(-30)
            const.centerY.equalToSuperview()
        }
        
        questionImage.snp.makeConstraints { (const) in
            const.width.height.equalTo(120)
        }
        buttonBack.snp.makeConstraints { (const) in
            const.width.equalToSuperview()
            const.height.equalTo(50)
        }
        
        yesButton.snp.makeConstraints { (const) in
            const.width.equalTo(100)
            const.height.equalTo(35)
            const.left.equalTo(0)
            const.centerY.equalToSuperview()
        }
        
        noButton.snp.makeConstraints { (const) in
            const.height.centerY.width.equalTo(yesButton)
            const.right.equalTo(0)
        }
        
        questionLabel.text = item.descr
        
        if item.isLast {
            buttonBack.isHidden = true
            questionImage.image = #imageLiteral(resourceName: "tick")
        }
        
        
    }
    
    @objc func gotoNext(sender: UIButton) {
        if sender == self.yesButton {
            self.pageController?.toNext(of: self.item, yes: true)
        }else{
            self.pageController?.toNext(of: self.item, yes: false)
        }
        
    }

}
