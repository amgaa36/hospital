//
//  DetailController.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/1/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import Presentr
import SwiftyJSON
import MessageUI
import SVProgressHUD

class DetailController: HViewController {

    var hospitalId = 0
    let galleryV = MCGalleryView()
    var imageUrls = [String]()
    var stackView = UIStackView()
    let timetableView = DetailItemView()
    let addressView = DetailItemView()
    let numberView = DetailItemView()
    let emailView = DetailItemView()
    let facebookView = DetailItemView()
    
    let turulButton = UIButton()
    let emchButton = UIButton()
    let tasagButton = UIButton()
    
    var presentr: Presentr!
    var json: JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(white: 0.95, alpha: 1)
        
//        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
//        fixedSpace.width = -16.0
//
//        let backButton = UIButton()
//        backButton.addTarget(self, action: #selector(self.backAction), for: .touchUpInside)
//        backButton.setImage(#imageLiteral(resourceName: "ic_chevron_left_white"), for: .normal)
//        backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//
//        navigationItem.leftBarButtonItems = [fixedSpace, UIBarButtonItem(customView: backButton)]
        
        
        
        presentr = Presentr(presentationType: .custom(width: .full, height: ModalSize.custom(size: 150), center: ModalCenterPosition.custom(centerPoint: CGPoint(x: view.center.x, y: view.frame.height - 75))))
        
        let scrollView = UIScrollView()
        scrollView.addSubview(stackView)
        view.addSubview(scrollView)
        
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 15
        

        galleryV.tappedImage = { index in
            let imageSlider = GalleryViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
            imageSlider.urls = self.imageUrls
            imageSlider.startIndex = index
            self.present(imageSlider, animated: true, completion: nil)
        }
        
        turulButton.backgroundColor = Constants.Color.mainOrange
        turulButton.setTitle("Төрөл", for: .normal)
        turulButton.setTitleColor(.white, for: .normal)
        turulButton.addTarget(self, action: #selector(bottomButtonAction(sender:)), for: .touchUpInside)
        view.addSubview(turulButton)
        
        emchButton.backgroundColor = Constants.Color.mainOrange
        emchButton.setTitle("Эмч", for: .normal)
        emchButton.setTitleColor(.white, for: .normal)
        emchButton.addTarget(self, action: #selector(bottomButtonAction(sender:)), for: .touchUpInside)
        view.addSubview(emchButton)
        
        tasagButton.backgroundColor = Constants.Color.mainOrange
        tasagButton.setTitle("Тасаг", for: .normal)
        tasagButton.setTitleColor(.white, for: .normal)
        tasagButton.addTarget(self, action: #selector(bottomButtonAction(sender:)), for: .touchUpInside)
        view.addSubview(tasagButton)
        
        
        
        stackView.snp.makeConstraints { (const) in
            const.top.left.equalTo(5)
            const.right.equalTo(-5)
            const.bottom.equalTo(-10)
            const.width.equalToSuperview().offset(-10)
        }
        
        scrollView.snp.makeConstraints { (const) in
            const.left.top.equalTo(0)
            const.width.equalToSuperview()
            const.bottom.equalTo(emchButton.snp.top)
        }
        
        turulButton.snp.makeConstraints { (const) in
            const.left.bottom.equalToSuperview()
            const.height.equalTo(50)
            const.width.equalToSuperview().multipliedBy(0.3)
        }
        
        emchButton.snp.makeConstraints { (const) in
            const.bottom.equalToSuperview()
            const.height.width.equalTo(turulButton)
            const.left.equalTo(turulButton.snp.right)
        }
        
        tasagButton.snp.makeConstraints { (const) in
            const.left.equalTo(emchButton.snp.right)
            const.bottom.right.equalToSuperview()
            const.height.equalTo(turulButton)
        }
        
        
        let api = APIProvider()
        SVProgressHUD.show()
        api.getDetail(hospitalId: hospitalId, success: { (json) in
            SVProgressHUD.dismiss()
            self.json = json
            let photos = json["medicalphotos"].arrayValue
            if photos.count > 0 {
                for special in photos {
                    self.imageUrls.append(Constants.Domain.resourceDomain + special["path"].stringValue)
                }
                self.galleryV.urls = self.imageUrls
                self.stackView.addArrangedSubview(self.galleryV)
                self.galleryV.snp.makeConstraints { const in
                    const.height.equalTo(self.view.frame.width * 2 / 3)
                }
                
            }
            let general = json["generalinfo"].arrayValue
            if general.count > 0 {
                let generalJson = general[0]
                if let table = generalJson["headline"].string {
                    let timeView = DetailDescView()
                    timeView.titleLabel.text = "Товч танилцуулга"
                    timeView.descLabel.text = table.replacingOccurrences(of: "; ", with: "\n")
                    self.stackView.addArrangedSubview(timeView)
                }
                
                if let table = generalJson["timeTable"].string {
                    let timeView = DetailItemView()
                    timeView.titleLabel.text = "Цагийн хуваарь"
                    timeView.descLabel.text = table.replacingOccurrences(of: "; ", with: "\n")
                    self.stackView.addArrangedSubview(timeView)
                }
                
                if let table = generalJson["address"].string {
                    let timeView = DetailItemView()
                    timeView.titleLabel.text = "Хаяг"
                    timeView.descLabel.text = table
                    self.stackView.addArrangedSubview(timeView)
                }
                
                if let table = generalJson["phone"].string {
                    let timeView = DetailItemView()
                    timeView.titleLabel.text = "Утасны дугаар"
                    timeView.descLabel.text = table
                    timeView.descLabel.labelType = .phone
                    self.stackView.addArrangedSubview(timeView)
                }
                if let table = generalJson["fbAddress"].string {
                    let timeView = DetailItemView()
                    timeView.titleLabel.text = "Facebook хаяг"
                    timeView.descLabel.text = table
                    timeView.descLabel.labelType = .web
                    self.stackView.addArrangedSubview(timeView)
                }
                if let table = generalJson["email"].string {
                    let timeView = DetailItemView()
                    timeView.titleLabel.text = "И-мэйл"
                    timeView.descLabel.labelType = .email
                    timeView.descLabel.text = table
                    timeView.descLabel.sendMailTo = { email in
                        if MFMailComposeViewController.canSendMail() {
                            let toRecipents = [email]
                            let mc:MFMailComposeViewController = MFMailComposeViewController()
                            mc.mailComposeDelegate = self
                            mc.setSubject("")
                            mc.setMessageBody("Сайн уу", isHTML: false)
                            mc.setToRecipients(toRecipents)
                            self.present(mc, animated: true, completion: nil)
                        }
                    }
                    self.stackView.addArrangedSubview(timeView)
                }
                
                if let table = generalJson["busStation"].string {
                    let timeView = DetailItemView()
                    timeView.titleLabel.text = "Автобус"
                    timeView.descLabel.text = table
                    self.stackView.addArrangedSubview(timeView)
                }
            }
        }) { (errorString) in
            AlertBuilder.sharedInstance.showError(message: errorString, title: "Уучлаарай")
            SVProgressHUD.dismiss()
            print(errorString)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bottomButtonAction(sender: UIButton){
        let addition = AdditionController()
        addition.json = self.json
        if sender == turulButton {
            addition.type = .turul
        }
        if sender == emchButton {
            addition.type = .emch
        }
        if sender == tasagButton {
            addition.type = .tasag
        }
        customPresentViewController(presentr, viewController: addition, animated: true, completion: nil)
    }
    
//    func backAction(){
//        self.navigationController?.popViewController(animated: true)
//    }

}

extension DetailController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
