//
//  MapViewController.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/3/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Presentr
import GoogleMaps

class MapViewController: HViewController, GMSMapViewDelegate {

    let mapView = MKMapView()
    var presenter: Presentr!
    var googleMap = GMSMapView()
    var markers = [GMSMarker]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = Presentr(presentationType: .dynamic(center: ModalCenterPosition.custom(centerPoint: view.center)))

//        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
//        fixedSpace.width = -16.0
        
//        let backButton = UIButton()
//        backButton.addTarget(self, action: #selector(self.backAction), for: .touchUpInside)
//        backButton.setImage(#imageLiteral(resourceName: "ic_chevron_left_white"), for: .normal)
//        backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//
//        navigationItem.leftBarButtonItems = [fixedSpace, UIBarButtonItem(customView: backButton)]
        
        let camera = GMSCameraPosition.camera(withLatitude: 47.9203, longitude: 106.917008, zoom: 13)
        googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        googleMap.delegate = self
        
        var topLeft = CLLocationCoordinate2DMake(-90, 180)
        var bottomRight = CLLocationCoordinate2DMake(90, -180)
        
        for hospital in HospitalDataManager.shared.hospitals {
            let marker = GMSMarker()
            marker.position = hospital.coordinate
            marker.map = self.googleMap
            marker.icon = #imageLiteral(resourceName: "marker")
            markers.append(marker)
            
            topLeft.longitude = fmin(topLeft.longitude, hospital.coordinate.longitude)
            topLeft.latitude = fmax(topLeft.latitude, hospital.coordinate.latitude)
            bottomRight.longitude = fmax(bottomRight.longitude, hospital.coordinate.longitude)
            bottomRight.latitude = fmin(bottomRight.latitude, hospital.coordinate.latitude)
        }
        
        
        let center = CLLocationCoordinate2DMake((topLeft.latitude + bottomRight.latitude) * 0.5, (bottomRight.longitude + topLeft.longitude) * 0.5)
        
        googleMap.animate(toLocation: center)
        view.addSubview(googleMap)

        googleMap.snp.makeConstraints { (const) in
            const.edges.equalToSuperview()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func backAction(){
//        self.navigationController?.popViewController(animated: true)
//    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let index = markers.index(of: marker) {
            let hospital = HospitalDataManager.shared.hospitals[index]
            let callOut = CallOutController()
            callOut.nameLabel.text = hospital.name
            callOut.phoneLabel.text = hospital.phone
            callOut.detailAction = {
                let detailController = DetailController()
                detailController.hospitalId = Int(hospital.id)!
                self.navigationController?.pushViewController(detailController, animated: true)
            }
            customPresentViewController(presenter, viewController: callOut, animated: true, completion: nil)
            
        }
        return true
    }

}
