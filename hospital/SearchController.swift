//
//  SearchController.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/8/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD


class SearchController: HViewController {

    let searchBar = UISearchBar()
    let resultTable = UITableView()
    let tableSource = HospitalTableDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        let backButton = UIButton()
        backButton.addTarget(self, action: #selector(self.backAction), for: .touchUpInside)
        backButton.setImage(#imageLiteral(resourceName: "ic_chevron_left_white"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = -16.0
        
        navigationItem.leftBarButtonItems = [fixedSpace, UIBarButtonItem(customView: backButton)]
        
        searchBar.placeholder = "Хайх"
        searchBar.delegate = self
        searchBar.barTintColor = .white
        searchBar.tintColor = .white
        searchBar.backgroundColor = .clear
        searchBar.backgroundImage = UIImage()
        searchBar.isTranslucent = true
        searchBar.setValue("Болих", forKey:"_cancelButtonText")
        navigationItem.titleView = searchBar
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        
        tableSource.controller = self
        resultTable.register(HospitalCell.self, forCellReuseIdentifier: Constants.CellId.hospitalList)
        resultTable.dataSource = tableSource
        resultTable.delegate = tableSource
        resultTable.tableFooterView = UIView()
        resultTable.separatorStyle = .none
        
        view.addSubview(resultTable)
        
        resultTable.snp.makeConstraints { (const) in
            const.edges.equalToSuperview()
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SearchController: UISearchBarDelegate {
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        filterTableView()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterTableView()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        filterTableView()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        resultTable.reloadData()
    }
    
    func filterTableView(){
        SVProgressHUD.show()
        var queryString = ""
        if let text = searchBar.text {
            queryString = text
        }
        
        let api = APIProvider()
        api.searchHospital(page: 1, name: queryString, success: { (json) in
            SVProgressHUD.dismiss()
            let arr = json["medical"].arrayValue
            var hospitals = [Hospital]()
            if arr.count > 0 {
                for hosp in arr {
                    hospitals.append(Hospital(json: hosp))
                }
                
            }
            self.tableSource.dataArray = hospitals
            self.resultTable.reloadData()
        }) { (errorString) in
            SVProgressHUD.dismiss()
            AlertBuilder.sharedInstance.showError(message: errorString, title: "Уучлаарай")
            print(errorString)
        }
        
    }
    
}


