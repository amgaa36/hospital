//
//  DiagnoseListController.swift
//  hospital
//
//  Created by Ankhbayar TS on 1/30/18.
//  Copyright © 2018 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON

struct DiagnoseListItem {
    let id: Int
    let name: String
}


class DiagnoseListController: HViewController, UITableViewDelegate, UITableViewDataSource {

    let tableView = UITableView()
    var itemList = [DiagnoseListItem]()
    var id: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Та доорх асуултуудаас сонгоно уу"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.register(DiagnoseListCell.self, forCellReuseIdentifier: "DiagnoseListCell")
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (const) in
            const.edges.equalToSuperview()
        }
        self.getData()
    }

    
    private func getData(){
        let api = APIProvider()
        api.getDiagnoseList(type: self.id, success: { (json) in
            let itemArray = json["data"].arrayValue
            if itemArray.count > 0 {
                for itemJson in itemArray {
                    let item = DiagnoseListItem(id: itemJson["id"].intValue, name: itemJson["name"].stringValue)
                    self.itemList.append(item)
                }
                self.tableView.reloadData()
            }
        }) { (errorString) in
            AlertBuilder.sharedInstance.showError(message: errorString, title: "Уучлаарай")
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiagnoseListCell") as! DiagnoseListCell
        let item = itemList[indexPath.row]
        cell.nameLabel.text = item.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = itemList[indexPath.row]
        let detail = DiagnoseDetailPageController()
        detail.id = item.id
        self.present(detail, animated: true, completion: nil)
    }
}
