//
//  SuvilagchCell.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/23/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import FoldingCell
import Cosmos

class SuvilagchCell: FoldingCell {
    
    let suvilagchImage = UIImageView()
    let nameLabel = UILabel()
    
    let containerImage = UIImageView()
    let containerName = UILabel()
    let callButton = UIButton()
    let reviewButton = UIButton()
    
    let turshilgaLabel = AttributeLabel()
    let chiglelLabel = AttributeLabel()
    let namtarLabel = AttributeLabel()
    let containerStack = UIStackView()
    
    let rate1 = CosmosView()
    let rate2 = CosmosView()
    
    var submitAction: (()->Void)!
    var reviewAction: (()->Void)!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        containerView = createContainerView()
        foregroundView = createForegroundView()
        itemCount = 3
        commonInit()
        backViewColor = UIColor(red: 40/255, green: 180/255, blue: 170/255, alpha: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
    private func createContainerView() -> UIView {
        
        let containerView = UIView(frame: .zero)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(containerView)
        
        containerView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8).isActive = true
        containerViewTop = containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 87)
        containerViewTop.isActive = true
        containerView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 280).isActive = true
        
        containerView.addShadow()
        containerView.backgroundColor = Constants.Color.mainGreen
        containerView.layer.cornerRadius = 5
        
        containerName.font = UIFont(name: "Roboto-Bold", size: 16)
        containerName.numberOfLines = 0
        
        containerImage.contentMode = .scaleAspectFill
        containerImage.clipsToBounds = true
        
        containerName.textColor = .white
        turshilgaLabel.textColor = .white
        chiglelLabel.textColor = .white
        namtarLabel.textColor = .white
        
        turshilgaLabel.numberOfLines = 0
        chiglelLabel.numberOfLines = 0
        namtarLabel.numberOfLines = 0
        
        callButton.backgroundColor = Constants.Color.mainOrange
        callButton.setTitle("Дуудлага өгөх", for: .normal)
        callButton.setTitleColor(.white, for: .normal)
        callButton.titleLabel?.font = UIFont(name: "Roboto", size: 17)
        callButton.layer.cornerRadius = 5
        callButton.addTarget(self, action: #selector(self.submitButtonAction), for: .touchUpInside)
        
        reviewButton.backgroundColor = Constants.Color.mainOrange
        reviewButton.setTitle("Үнэлэх", for: .normal)
        reviewButton.setTitleColor(.white, for: .normal)
        reviewButton.titleLabel?.font = UIFont(name: "Roboto", size: 17)
        reviewButton.layer.cornerRadius = 5
        reviewButton.addTarget(self, action: #selector(self.reviewButtonAction), for: .touchUpInside)
        
        containerStack.axis = .vertical
        containerStack.distribution = .fillProportionally
        containerStack.spacing = 2
        containerStack.alignment = .fill
        
        containerStack.addArrangedSubview(turshilgaLabel)
        containerStack.addArrangedSubview(chiglelLabel)
        containerStack.addArrangedSubview(namtarLabel)
        
        
        rate2.rating = 4.5
        rate2.settings.emptyBorderColor = .white
        rate2.settings.emptyBorderWidth = 1
        rate2.settings.filledColor = .white
        rate2.settings.filledBorderColor = .white
        rate2.settings.updateOnTouch = false
        
        containerView.addSubview(containerImage)
        containerView.addSubview(containerName)
        containerView.addSubview(containerStack)
        containerView.addSubview(callButton)
        containerView.addSubview(reviewButton)
        containerView.addSubview(rate2)
        
        
        containerImage.snp.makeConstraints { (const) in
            const.left.top.equalTo(10)
            const.width.height.equalTo(80)
        }
        
        containerName.snp.makeConstraints { (const) in
            const.left.equalTo(containerImage.snp.right).offset(10)
            const.top.equalTo(containerImage)
            const.right.equalToSuperview().offset(-10)
        }
        
        rate2.snp.makeConstraints { (const) in
            const.left.equalTo(containerName)
            const.top.equalTo(containerName.snp.bottom).offset(10)
            
        }
        
        containerStack.snp.makeConstraints { (const) in
            const.left.equalTo(containerImage)
            const.top.equalTo(containerImage.snp.bottom).offset(10)
            const.right.equalTo(containerName)
        }
        
        callButton.snp.makeConstraints { (const) in
            const.left.equalTo(containerStack)
            const.right.equalTo(containerView.snp.centerX).offset(-5)
            const.height.equalTo(50)
            const.bottom.equalTo(-10)
        }
        
        reviewButton.snp.makeConstraints { (const) in
            const.left.equalTo(containerView.snp.centerX).offset(5)
            const.height.bottom.equalTo(callButton)
            const.right.equalTo(containerStack)
        }
        
        containerView.layoutIfNeeded()
        
        return containerView
    }
    
    
    private func createForegroundView() -> RotatedView {
        let foregroundView = RotatedView(frame: .zero)
        foregroundView.backgroundColor = .white
        foregroundView.layer.borderColor = UIColor(white: 233/255, alpha: 1).cgColor
        foregroundView.layer.borderWidth = 1
        foregroundView.addShadow()
        foregroundView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(foregroundView)
        
        suvilagchImage.contentMode = .scaleAspectFill
        suvilagchImage.clipsToBounds = true
        
        nameLabel.font = UIFont(name: "Roboto-Bold", size: 16)
        nameLabel.textColor = .black
        nameLabel.numberOfLines = 0
        
        rate1.rating = 4.5
        rate1.settings.emptyBorderColor = Constants.Color.mainGreen
        rate1.settings.emptyBorderWidth = 1
        rate1.settings.filledColor = Constants.Color.mainGreen
        rate1.settings.filledBorderColor = Constants.Color.mainGreen
        rate1.settings.updateOnTouch = false
        
        foregroundView.addSubview(suvilagchImage)
        foregroundView.addSubview(nameLabel)
        foregroundView.addSubview(rate1)
        foregroundView.layer.cornerRadius = 5
        
        foregroundView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8).isActive = true
        foregroundViewTop = foregroundView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12)
        foregroundViewTop.isActive = true
        foregroundView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8).isActive = true
        foregroundView.heightAnchor.constraint(equalToConstant: 75).isActive = true
        
        suvilagchImage.snp.makeConstraints { (const) in
            const.left.equalTo(17)
            const.top.equalTo(5)
            const.width.equalTo(65)
            const.height.equalTo(65)
        }
        
        nameLabel.snp.makeConstraints { (const) in
            const.top.equalTo(suvilagchImage)
            const.right.equalTo(-10)
            const.left.equalTo(suvilagchImage.snp.right).offset(10)
            const.bottom.lessThanOrEqualTo(-10)
        }
        
        rate1.snp.makeConstraints { (const) in
            const.top.equalTo(nameLabel.snp.bottom).offset(10)
            const.left.right.equalTo(nameLabel)
        }
        
        return foregroundView
    }
    
    
    @objc func submitButtonAction(){
        self.submitAction()
    }
    
    @objc func reviewButtonAction(){
        self.reviewAction()
    }
}
