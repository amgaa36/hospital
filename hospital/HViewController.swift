//
//  HViewController.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/25/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SVProgressHUD

class HViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        if let navigation = self.navigationController {
            if navigation.viewControllers.count > 1 {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: #selector(backAction))
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SVProgressHUD.dismiss()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
}
