//
//  HospitalDataManager.swift
//  hospital
//
//  Created by user04 on 8/28/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SVProgressHUD

class HospitalDataManager: NSObject {

    static let shared: HospitalDataManager = HospitalDataManager()
    var hospitals = [Hospital]()
    var hospitalUpdated: (()->Void)?
    var type: HospitalType?
    var selectedLabs = [Int]()
    var currentPage = 0
    var totalPage = 1
    
    
    func labChanged(){
        currentPage = 0
        totalPage = 1
        self.hospitals.removeAll()
        self.retrieveMoreHospital()
    }
    
    
    func retrieveMoreHospital(){
        if let type = self.type {
            SVProgressHUD.show()
            let apiProvider = APIProvider()
            if currentPage < totalPage {
                apiProvider.searchHospital(page: currentPage + 1, success: { (json) in
                    SVProgressHUD.dismiss()
                    self.totalPage = json["pagecount"].intValue
                    self.currentPage = json["page"].intValue
                    let medical = json["medical"].arrayValue
                    if medical.count > 0 {
                        for hosp in medical {
                            self.hospitals.append(Hospital(json: hosp))
                        }
                        if let handler = self.hospitalUpdated {
                            handler()
                        }
                    }
                    
                }) { (errorString) in
                    SVProgressHUD.dismiss()
                    print(errorString)
                }
            }
        }
    }
    
    
}
