//
//  DiagnoseDetailPageController.swift
//  hospital
//
//  Created by Ankhbayar TS on 1/31/18.
//  Copyright © 2018 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON

struct DiagnoseQuestionItem {
    let id: Int
    let isLast: Bool
    let isFirst: Bool
    let descr: String
    let yesId: Int
    let noId: Int
}

class DiagnoseDetailPageController: UINavigationController {

    var id: Int!
    var arrayDic = [String: DiagnoseQuestionItem]()
    var currentController: DiagnoseQuestionController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = Constants.Color.mainGreen
        self.navigationBar.tintColor = .white
        self.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont(name: "Roboto", size: 17)!
        ]
        self.getData()
    }

    
    
    func toNext(of item: DiagnoseQuestionItem, yes: Bool){
        
        let nextId = yes ? item.yesId : item.noId
        if let nextItem = self.arrayDic["\(nextId)"] {
            let nextController = DiagnoseQuestionController()
            nextController.item = nextItem
            nextController.pageController = self
            self.setViewControllers([nextController], animated: true)
        }else{
            AlertBuilder.sharedInstance.showError(message: "Алдаа гарлаа", title: "Уучлаарай")
        }
    }
    
    private func getData(){
        let api = APIProvider()
        api.getDiagnoseDetail(diagnose: self.id, success: { (json) in
            let data = json["data"].arrayValue
            if data.count > 0 {
                for item in data {
                    let questionItem = DiagnoseQuestionItem(id: item["id"].intValue, isLast: item["isLast"].boolValue, isFirst: item["isFirst"].boolValue, descr: item["descr"].stringValue, yesId: item["YesId"].intValue, noId: item["NoId"].intValue)
                    if questionItem.isFirst {
                        self.arrayDic["first"] = questionItem
                    }else{
                        self.arrayDic[item["id"].stringValue] = questionItem
                    }
                    
                }
                
                let nextController = DiagnoseQuestionController()
                nextController.pageController = self
                nextController.item = self.arrayDic["first"]
                self.setViewControllers([nextController], animated: false)
            }else{
                AlertBuilder.sharedInstance.showError(message: "Асуулт олдсонгүй.", title: "Уучлаарай")
            }
        }) { (errorString) in
            AlertBuilder.sharedInstance.showError(message: errorString, title: "Уучлаарай")
        }
    }
    
    
    

}
