//
//  DetailDescView.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/14/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SnapKit

class DetailDescView: UIView {

    let titleLabel = UILabel()
    let descLabel = UILabel()
    let button = UIButton()
    var expanded = false
    var descHeightConstriant: Constraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        titleLabel.font = Constants.String.mainFont
        titleLabel.textColor = Constants.Color.mainGreen
        titleLabel.numberOfLines = 0
        
        descLabel.numberOfLines = 0
        descLabel.textColor = .gray
        descLabel.font = UIFont.systemFont(ofSize: 13)
        descLabel.textAlignment = .justified
        
        button.setTitle("цааш унших", for: .normal)
        button.addTarget(self, action: #selector(toggle), for: .touchDown)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.backgroundColor = UIColor(white: 0.95, alpha: 1)
        
        addSubview(titleLabel)
        addSubview(descLabel)
        addSubview(button)
        
        titleLabel.snp.makeConstraints { (const) in
            const.top.equalTo(10)
            const.left.equalTo(20)
            const.right.equalTo(-20)
        }
        
        descLabel.snp.makeConstraints { (const) in
            const.top.equalTo(titleLabel.snp.bottom).offset(5)
            const.left.right.equalTo(titleLabel)
            descHeightConstriant = const.height.lessThanOrEqualTo(40).priority(1000).constraint
        }
        
        button.snp.makeConstraints { (const) in
            const.width.equalTo(120)
            const.height.equalTo(30)
            const.bottom.equalTo(-10)
            const.top.equalTo(descLabel.snp.bottom).offset(5)
            const.centerX.equalToSuperview()
        }
        
        self.layer.cornerRadius = 5
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toggle(){
        if expanded {
            descLabel.snp.makeConstraints { (const) in
                const.top.equalTo(titleLabel.snp.bottom).offset(5)
                const.left.right.equalTo(titleLabel)
                descHeightConstriant = const.height.lessThanOrEqualTo(40).priority(1000).constraint
            }
            button.setTitle("цааш унших", for: .normal)
            expanded = false
        }else{
            descLabel.snp.remakeConstraints { (const) in
                const.top.equalTo(titleLabel.snp.bottom).offset(5)
                const.left.right.equalTo(titleLabel)
            }
            button.setTitle("хураах", for: .normal)
            expanded = true
        }
        
        UIView.animate(withDuration: 0.5) {
            self.descLabel.layoutIfNeeded()
        }
    }

}
