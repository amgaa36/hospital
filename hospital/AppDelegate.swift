//
//  AppDelegate.swift
//  hospital
//
//  Created by Ankhbayar TS on 8/24/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import GoogleMaps
import UserNotifications
import Alamofire
import Fabric
import Crashlytics
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        GMSServices.provideAPIKey("AIzaSyB3K-Cp37GfxySmJPDoO51oFFMwW_--8S4")
        application.applicationIconBadgeNumber = 0
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                if granted {
                    
//                    application.registerForRemoteNotifications()
                }
            }
        } else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }

        
        
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
//        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("default.realm")
        
        Realm.Configuration.defaultConfiguration = config

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        UserDefaults.standard.set(deviceTokenString, forKey: "devtoken")
        var deviceId = ""
        if let devid = UIDevice.current.identifierForVendor?.uuidString{
            deviceId = devid
        }
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let deviceName = UIDevice.current.name
        let deviceModel = UIDevice.current.model
        let deviceOS = UIDevice.current.systemVersion
        
        let urlString = "http://emneleg.mn/api/device/new"
        
        let param: [String: Any] = ["status": appVersion, "deviceId": deviceId, "deviceToken": deviceTokenString.lowercased(), "deviceName": deviceName, "deviceModel": deviceModel, "osVersion": deviceOS, "isIOS": true]
        if let url = URL(string: urlString) {
            print("sending app delegate request to \(urlString)")
            Alamofire.request(url, method: .get, parameters: param).responseJSON(completionHandler: { (response) in
                print(response.value ?? "fdafafa")
            })
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
        
    }

}

