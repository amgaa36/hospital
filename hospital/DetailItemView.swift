//
//  DetailItemView.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/2/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class DetailItemView: UIView {

    let titleLabel = UILabel()
    let descLabel = LinkLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        titleLabel.font = Constants.String.mainFont
        titleLabel.textColor = Constants.Color.mainGreen
        titleLabel.numberOfLines = 0
        
        descLabel.numberOfLines = 0
        descLabel.textColor = .gray
        descLabel.font = UIFont.systemFont(ofSize: 13)
        
        addSubview(titleLabel)
        addSubview(descLabel)
        
        titleLabel.snp.makeConstraints { (const) in
            const.top.equalTo(10)
            const.left.equalTo(20)
            const.right.equalTo(-20)
        }
        
        descLabel.snp.makeConstraints { (const) in
            const.top.equalTo(titleLabel.snp.bottom).offset(5)
            const.left.right.equalTo(titleLabel)
            const.bottom.equalTo(-10)
        }
        
        self.layer.cornerRadius = 5
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
