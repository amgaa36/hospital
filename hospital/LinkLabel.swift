//
//  LinkLabel.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/10/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit


enum LinkLabelType {
    case phone
    case web
    case email
    case none
}

class LinkLabel: UILabel {
    
    var labelType: LinkLabelType = .none
    var sendMailTo: ((String)->Void)?
    init() {
        super.init(frame: .zero)
        isUserInteractionEnabled = true
        let tgr = UITapGestureRecognizer(target: self, action: #selector(tapped))
        addGestureRecognizer(tgr)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tapped(){
        if let text = self.text {
            if labelType == .phone {
                let phones = text.components(separatedBy: ";")
                if phones.count > 0 {
                    let phoneNumber = phones[0]
                    if let url = URL(string: "tel://" + phoneNumber) {
                        if #available(iOS 11.0, *){
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }else{
                            UIApplication.shared.openURL(url)
                        }
                        
                    }
                    
                }
            }else if labelType == .web {
                if let url = URL(string: text) {
                    if #available(iOS 11.0, *){
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }else{
                        UIApplication.shared.openURL(url)
                    }
                }
            }else if labelType == .email {
                if let handler = self.sendMailTo {
                    handler(text)
                }
            }
        }
    }
    
}
