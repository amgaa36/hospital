//
//  HospitalType.swift
//  hospital
//
//  Created by Ankhbayar TS on 9/2/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SwiftyJSON

struct HospitalType {

    var name: String
    var photo: String
    var id: String
    
    init(json: JSON) {
        name = json["name"].stringValue
        id = json["id"].stringValue
        photo = Constants.Domain.resourceDomain + json["img"].stringValue
    }
    
}
