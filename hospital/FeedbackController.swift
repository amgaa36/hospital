//
//  FeedbackController.swift
//  hospital
//
//  Created by Ankhbayar TS on 12/12/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SVProgressHUD

class FeedbackController: HViewController {

    let bodyTextView = UITextView()
    let submitButton = UIButton()
    let titleLabel = UILabel()
    let bodyBack = UIView()
    var nurse: Nurse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        titleLabel.textColor = .black
        titleLabel.font = UIFont(name: "Roboto", size: 16)
        titleLabel.text = "Санал сэтгэгдэл"
        
        bodyBack.layer.borderColor = UIColor.lightGray.cgColor
        bodyBack.layer.cornerRadius = 4
        bodyBack.layer.borderWidth = 0.5
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(tappedAction))
        view.addGestureRecognizer(tgr)
        
        
        submitButton.setTitle("Илгээх", for: .normal)
        submitButton.setTitleColor(.white, for: .normal)
        submitButton.backgroundColor = Constants.Color.mainOrange
        submitButton.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
        submitButton.layer.cornerRadius = 3
        
        
        
        view.addSubview(titleLabel)
        view.addSubview(bodyBack)
        bodyBack.addSubview(bodyTextView)
        view.addSubview(submitButton)
        
        titleLabel.snp.makeConstraints { (const) in
            const.left.equalTo(30)
            const.top.equalTo(30)
        }
        
        bodyBack.snp.makeConstraints { (const) in
            const.left.equalTo(titleLabel)
            const.width.equalTo(240)
            const.height.equalTo(200)
            const.right.equalTo(-30)
            const.top.equalTo(titleLabel.snp.bottom).offset(20)
        }
        
        submitButton.snp.makeConstraints { (const) in
            const.top.equalTo(bodyBack.snp.bottom).offset(20)
            const.width.equalTo(120)
            const.height.equalTo(40)
            const.right.equalTo(bodyBack)
            const.bottom.equalTo(-30)
        }
        
        bodyTextView.snp.makeConstraints { (const) in
            const.edges.equalToSuperview().inset(UIEdgeInsetsMake(5, 5, 5, 5))
        }
        
    }
    
    @objc func tappedAction(){
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func submitButtonAction(){
        SVProgressHUD.show()
        let api = APIProvider()
        if self.bodyTextView.text != "" {
            SVProgressHUD.dismiss()
            api.sendFeedback(nurse: self.nurse, content: self.bodyTextView.text, success: { (json) in
                if json["status"].stringValue == "ok" {
                    AlertBuilder.sharedInstance.showSuccess(message: "Баярлалаа", title: "Амжилттай")
                    self.dismiss(animated: true, completion: nil)
                }else{
                    AlertBuilder.sharedInstance.showError(message: "Хүсэлтэнд алдаа гарлаа", title: "Уучлаарай")
                }
            }) { (errorString) in
                AlertBuilder.sharedInstance.showError(message: errorString, title: "Уучлаарай")
            }
        }else{
            SVProgressHUD.dismiss()
            AlertBuilder.sharedInstance.showError(message: "Хүсэлтээ бөглөнө үү!", title: "Уучлаарай")
        }
        
    }

}
