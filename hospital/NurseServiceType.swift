//
//  NurseServiceType.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/23/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import RealmSwift

class NurseServiceType: Object {

    @objc dynamic var name: String = ""
    @objc dynamic var id: Int = 0
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}


class NursePosition: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var id: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
