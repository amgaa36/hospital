//
//  FilterController.swift
//  hospital
//
//  Created by Ankhbayar TS on 9/30/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class Lab {
    let id: Int
    let name: String
    var selected: Bool = false {
        didSet{
            if selected == false {
                if let ind = HospitalDataManager.shared.selectedLabs.index(of: id) {
                    HospitalDataManager.shared.selectedLabs.remove(at: ind)
                }
            }else{
                if !HospitalDataManager.shared.selectedLabs.contains(id) {
                    HospitalDataManager.shared.selectedLabs.append(id)
                }
            }
        }
    }
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
        self.selected = HospitalDataManager.shared.selectedLabs.contains(self.id)
    }
}

class FilterController: UIViewController {

    let labTable = UITableView()
    var types = [Lab]()
    var dismissBlock: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
        let backView = UIView()
        backView.backgroundColor = UIColor(white: 0.1, alpha: 0.3)
        view.addSubview(backView)
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(tapped))
        backView.addGestureRecognizer(tgr)
        
        
        labTable.register(LabViewCell.self, forCellReuseIdentifier: "labCell")
        labTable.delegate = self
        labTable.dataSource = self
        view.addSubview(labTable)
        
        backView.snp.makeConstraints { (const) in
            const.edges.equalToSuperview()
        }
        labTable.snp.makeConstraints { (const) in
            const.left.right.equalToSuperview()
            const.bottom.equalToSuperview()
            const.top.equalTo(view.snp.centerY)
        }
        
        let api = APIProvider()
        api.serviceList(success: { (json) in
            let labs = json["labType"].arrayValue
            if labs.count > 0 {
                for labJson in labs {
                    let newLab = Lab(id: labJson["id"].intValue, name: labJson["name"].stringValue)
                    self.types.append(newLab)
                }
                self.labTable.reloadData()
            }
        }) { (errorString) in
            print(errorString)
        }
        
    }

    func tapped (){
        self.dismiss(animated: true) {
            if let completion = self.dismissBlock {
                completion()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        HospitalDataManager.shared.labChanged()
    }

}

extension FilterController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "labCell") as! LabViewCell
        let lab = types[indexPath.row]
        cell.setData(data: lab)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! LabViewCell
        let lab = types[indexPath.row]
        lab.selected = !lab.selected
        cell.change(selected: lab.selected)
    }
}
