//
//  DiagnoseListCell.swift
//  hospital
//
//  Created by Ankhbayar TS on 1/30/18.
//  Copyright © 2018 Amgaa. All rights reserved.
//

import UIKit

class DiagnoseListCell: UITableViewCell {

    let nameLabel = UILabel()
    let backView = UIView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        nameLabel.textAlignment = .center
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont(name: "Roboto", size: 13)
        nameLabel.textColor = Constants.Color.mainGreen
        
        backView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        self.contentView.addSubview(backView)
        self.contentView.addSubview(nameLabel)
        
        
        backView.snp.makeConstraints { (const) in
            const.edges.equalToSuperview().inset(UIEdgeInsetsMake(2, 4, 2, 4))
        }
        nameLabel.snp.makeConstraints { (const) in
            const.edges.equalTo(backView).inset(UIEdgeInsetsMake(5, 5, 5, 5))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
