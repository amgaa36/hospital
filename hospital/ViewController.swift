//
//  ViewController.swift
//  hospital
//
//  Created by Ankhbayar TS on 8/24/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: HViewController {
    
    let hospitalTable = UITableView()
    var loadingMore = false
    var mapButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = HospitalDataManager.shared.type?.name
//        let backButton = UIButton()
//        backButton.addTarget(self, action: #selector(self.backAction), for: .touchUpInside)
//        backButton.setImage(#imageLiteral(resourceName: "ic_chevron_left_white"), for: .normal)
//        backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        let searchButton = UIButton()
        searchButton.addTarget(self, action: #selector(self.searchAction), for: .touchUpInside)
        searchButton.setImage(#imageLiteral(resourceName: "ic_search_white"), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        let filterButton = UIButton()
        filterButton.addTarget(self, action: #selector(self.filterAction), for: .touchUpInside)
        filterButton.setImage(#imageLiteral(resourceName: "ic_tune_white"), for: .normal)
        filterButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: searchButton),UIBarButtonItem(customView: filterButton)]
        
        hospitalTable.register(HospitalCell.self, forCellReuseIdentifier: Constants.CellId.hospitalList)
        let tableSource = HospitalTableDataSource()
        tableSource.reachedBottom = {
            if !self.loadingMore {
                self.loadingMore = true
                HospitalDataManager.shared.retrieveMoreHospital()
            }
        }
        tableSource.controller = self
        
        hospitalTable.dataSource = tableSource
        hospitalTable.delegate = tableSource
        hospitalTable.tableFooterView = UIView()
        hospitalTable.separatorStyle = .none
        
        view.addSubview(hospitalTable)
        hospitalTable.snp.makeConstraints { (const) in
            const.edges.equalTo(view)
        }
        
        HospitalDataManager.shared.hospitalUpdated = {
            tableSource.dataArray = HospitalDataManager.shared.hospitals
            self.hospitalTable.reloadData()
            self.loadingMore = false
        }
        HospitalDataManager.shared.retrieveMoreHospital()
        
        
        view.addSubview(mapButton)
        mapButton.backgroundColor = Constants.Color.mainGreen
        mapButton.layer.cornerRadius = 25
        mapButton.addTarget(self, action: #selector(showMap), for: .touchUpInside)
        mapButton.setImage(#imageLiteral(resourceName: "ic_place_white"), for: .normal)
        mapButton.snp.makeConstraints { (const) in
            const.right.bottom.equalTo(-20)
            const.width.height.equalTo(50)
        }
    }
    
    
    func showMap(){
        let mapViewController = MapViewController()
        self.navigationController?.pushViewController(mapViewController, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func backAction(){
//        self.navigationController?.popViewController(animated: true)
//    }

    func searchAction(){
        let searchController = SearchController()
        navigationController?.pushViewController(searchController, animated: true)
    }
    
    func filterAction(){
        let filterController = FilterController()
        filterController.modalPresentationStyle = .overCurrentContext
        filterController.definesPresentationContext = true
        filterController.providesPresentationContextTransitionStyle = true
        self.present(filterController, animated: true, completion: nil)
    }

}

