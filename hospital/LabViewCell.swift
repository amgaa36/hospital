//
//  LabViewCell.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/1/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class LabViewCell: UITableViewCell {

    private let nameLabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        nameLabel.textColor = .gray
        nameLabel.font = Constants.String.mainFont
        contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (const) in
            const.left.equalTo(20)
            const.top.bottom.right.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(data: Lab){
        nameLabel.text = data.name
        change(selected: data.selected)
    }
    
    public func change(selected: Bool){
        print(selected)
        if selected {
            self.backgroundColor = UIColor.lightGray
        }else{
            self.backgroundColor = .white
        }
    }
    
}
