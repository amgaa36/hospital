//
//  CallOutController.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/3/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class CallOutController: UIViewController {

    let nameLabel = UILabel()
    let phoneLabel = LinkLabel()
    let phoneImage = UIImageView()
    let detailButton = UIButton()
    var detailAction: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        nameLabel.font = UIFont.systemFont(ofSize: 17)
        nameLabel.numberOfLines = 2
        nameLabel.textAlignment = .center
        view.addSubview(nameLabel)
        
        phoneImage.image = #imageLiteral(resourceName: "ic_phone")
        phoneImage.contentMode = .scaleAspectFit
        view.addSubview(phoneImage)
        
        phoneLabel.numberOfLines = 0
        phoneLabel.textColor = UIColor.gray
        phoneLabel.font = UIFont.systemFont(ofSize: 14)
        phoneLabel.setContentCompressionResistancePriority(1000, for: .vertical)
        phoneLabel.labelType = .phone
        view.addSubview(phoneLabel)
        
        
        
        detailButton.backgroundColor = Constants.Color.mainGreen
        detailButton.setTitle("Дэлгэрэнгүй", for: .normal)
        detailButton.setTitleColor(.white, for: .normal)
        detailButton.addTarget(self, action: #selector(detailPressed), for: .touchUpInside)
        view.addSubview(detailButton)
        
        let lineView = UIView()
        lineView.backgroundColor = UIColor.lightGray
        view.addSubview(lineView)
        
        nameLabel.snp.makeConstraints { (const) in
            const.left.top.equalTo(10)
            const.right.equalTo(-10)
            const.width.equalTo(200)
            const.height.equalTo(50)
        }
        
        lineView.snp.makeConstraints { (const) in
            const.left.right.equalTo(0)
            const.height.equalTo(1)
            const.top.equalTo(nameLabel.snp.bottom)
        }
        
        phoneLabel.snp.makeConstraints { (const) in
            const.top.equalTo(nameLabel.snp.bottom).offset(15)
            const.right.equalTo(nameLabel)
            const.left.equalTo(phoneImage.snp.right)
        }
        phoneImage.snp.makeConstraints { (const) in
            const.height.equalTo(25)
            const.width.equalTo(30)
            const.centerY.equalTo(phoneLabel)
            const.left.equalTo(10)
        }
        detailButton.snp.makeConstraints { (const) in
            const.left.right.equalToSuperview()
            const.height.equalTo(40)
            const.top.equalTo(phoneLabel.snp.bottom).offset(15)
            const.bottom.equalTo(0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func detailPressed(){
        self.dismiss(animated: true) {
            if let handler = self.detailAction {
                handler()
            }
        }
    }

    
    
    

}
