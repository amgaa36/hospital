//
//  HomeGalleryDetailView.swift
//  hospital
//
//  Created by Ankhbayar TS on 10/9/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class HomeGalleryDetailView: MCGalleryDetailView {

    let phoneButton = UIButton()
    let nameLabel = UILabel()
    
    override init(frame: CGRect, url: String) {
        super.init(frame: frame, url: url)
        
        nameLabel.textColor = .white
        nameLabel.font = UIFont.systemFont(ofSize: 16)
        nameLabel.textAlignment = .center
        
        addSubview(nameLabel)
        
        imageView.layer.cornerRadius = 40
        imageView.backgroundColor = .white
        
        phoneButton.setTitleColor(.white, for: .normal)
        phoneButton.addTarget(self, action: #selector(callAction), for: .touchDown)
        phoneButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        
        addSubview(phoneButton)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: frame.width / 2 - 40, y: 20, width: 80, height: 80)
        nameLabel.frame = CGRect(x: frame.width / 2 - 120, y: frame.height - 80, width: 240, height: 30)
        phoneButton.frame = CGRect(x: frame.width / 2 - 50, y: frame.height - 50, width: 100, height: 30)
        
    }
    
    func callAction(){
        if let text = phoneButton.titleLabel?.text {
            let seperatedText = text.components(separatedBy: ";")
            let phoneNumber = seperatedText[0]
            if let url = URL(string: "tel://" + phoneNumber) {
                if #available(iOS 11.0, *){
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }else{
                    UIApplication.shared.openURL(url)
                }
                
            }
        }
    }
    
}
