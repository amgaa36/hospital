//
//  ResultController.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/24/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class ResultController: UIViewController {

    let headerLabel = UILabel()
    let footerLabel = UILabel()
    let bodyLabel = UILabel()
    let infoLabel = UILabel()
    let phoneLabel = UILabel()
    let closeButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.layer.cornerRadius = 6
        
        headerLabel.font = UIFont(name: "Roboto", size: 18)
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = 0
        
        infoLabel.font = UIFont(name: "Roboto", size: 14)
        infoLabel.numberOfLines = 0
        
        bodyLabel.font = UIFont(name: "Roboto-Light", size: 14)
        bodyLabel.numberOfLines = 0
        
        phoneLabel.font = UIFont(name: "Roboto", size: 14)
        phoneLabel.textAlignment = .center
        
        footerLabel.numberOfLines = 0
        footerLabel.font = UIFont(name: "Roboto", size: 14)
        footerLabel.textAlignment = .center
        
        closeButton.setTitle("Хаах", for: .normal)
        closeButton.setTitleColor(.white, for: .normal)
        closeButton.backgroundColor = Constants.Color.mainOrange
        closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
        closeButton.layer.cornerRadius = 3

        view.addSubview(headerLabel)
        view.addSubview(infoLabel)
        view.addSubview(bodyLabel)
        view.addSubview(phoneLabel)
        view.addSubview(footerLabel)
        view.addSubview(closeButton)
        
        headerLabel.snp.makeConstraints { (const) in
            const.top.equalTo(15)
            const.right.left.equalToSuperview()
            const.width.equalTo(280)
        }
        
        bodyLabel.snp.makeConstraints { (const) in
            const.left.equalTo(20)
            const.right.equalTo(-20)
            const.top.equalTo(headerLabel.snp.bottom).offset(10)
        }
        
        infoLabel.snp.makeConstraints { (const) in
            const.left.right.equalTo(bodyLabel)
            const.top.equalTo(bodyLabel.snp.bottom).offset(10)
        }
        footerLabel.snp.makeConstraints { (const) in
            const.left.right.equalTo(infoLabel)
            const.top.equalTo(infoLabel.snp.bottom).offset(10)
        }
        
        closeButton.snp.makeConstraints { (const) in
            const.centerX.equalToSuperview()
            const.width.equalTo(150)
            const.height.equalTo(40)
            const.top.equalTo(footerLabel.snp.bottom).offset(20)
            const.bottom.equalTo(-30)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func closeButtonAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
