//
//  HomeMenuController.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/18/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import RealmSwift

class HomeMenuController: UIViewController {

    let stackView = UIStackView()
    let hospitalButton = HomeMenuButton(name: "Эмнэлэг", icon: #imageLiteral(resourceName: "hospitalIcon"))
    let suvilagchButton = HomeMenuButton(name: "Сувилагч", icon: #imageLiteral(resourceName: "suvilagchIcon"))
    let otherButton = HomeMenuButton(name: "Цахим эмч", icon: #imageLiteral(resourceName: "doctor_case"))
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 20
        
        hospitalButton.addTarget(self, action: #selector(buttonPressDown(sender:)), for: .touchDown)
        hospitalButton.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        
        suvilagchButton.addTarget(self, action: #selector(buttonPressDown(sender:)), for: .touchDown)
        suvilagchButton.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        
        otherButton.addTarget(self, action: #selector(buttonPressDown(sender:)), for: .touchDown)
        otherButton.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        
        stackView.addArrangedSubview(hospitalButton)
        stackView.addArrangedSubview(suvilagchButton)
        stackView.addArrangedSubview(otherButton)
        
        view.addSubview(stackView)
        stackView.snp.makeConstraints { (const) in
            const.edges.equalToSuperview().inset(UIEdgeInsetsMake(20, 20, 20, 20))
        }
        
        
        let apiProvider = APIProvider()
        
        apiProvider.getSuvilagchType(success: { (json) in
            let typeArrayJson = json["data"].arrayValue
            let realm = try! Realm()
            for typeJson in typeArrayJson {
                let serviceType = NurseServiceType()
                serviceType.id = typeJson["id"].intValue
                serviceType.name = typeJson["name"].stringValue
                try! realm.write {
                    realm.add(serviceType, update: true)
                }
            }
        }) { (errorString) in
            print(errorString)
        }
        
        apiProvider.getSuvilagchPosition(success: { (json) in
            let typeArrayJson = json["data"].arrayValue
            let realm = try! Realm()
            for typeJson in typeArrayJson {
                let position = NursePosition()
                position.id = typeJson["id"].intValue
                position.name = typeJson["name"].stringValue
                try! realm.write {
                    realm.add(position, update: true)
                }
            }
        }) { (errorString) in
            print(errorString)
        }
        
    }

    @objc func buttonPressDown(sender: HomeMenuButton) {
        sender.layer.borderColor = Constants.Color.mainGreen.cgColor
    }
    
    @objc func buttonAction(sender: HomeMenuButton) {
        sender.layer.borderColor = UIColor(white: 233/255, alpha: 1).cgColor
        
        switch sender {
        case hospitalButton:
            self.navigationController?.pushViewController(HomeViewController(), animated: true)
        case suvilagchButton:
            self.navigationController?.pushViewController(SuvilagchController(), animated: true)
        case otherButton:
            self.navigationController?.pushViewController(DiagnoseTypeController(), animated: true)
        default:
            return
        }
    }
    

}
