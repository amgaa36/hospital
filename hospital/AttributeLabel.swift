//
//  AttributeLabel.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/24/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class AttributeLabel: UILabel {

    var keyFont: UIFont = UIFont(name: "Roboto-Bold", size: 14)!
    var valueFont: UIFont = UIFont(name: "Roboto", size: 14)!
    
    func setString(key: String, value: String?){
        let mutableString = NSMutableAttributedString()
        let keyPart = key + ": "
        let keyMutableString = NSAttributedString(string: keyPart, attributes: [NSFontAttributeName: self.keyFont])
        mutableString.append(keyMutableString)
        if let value = value {
            let valueMutableString = NSAttributedString(string: value, attributes: [NSFontAttributeName: self.valueFont])
            mutableString.append(valueMutableString)
        }
        
        self.attributedText = mutableString
    }
    
    

}
