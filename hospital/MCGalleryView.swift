//
//  MCGalleryView.swift
//  MyanCashAgent
//
//  Created by moco on 10/26/16.
//  Copyright © 2016 Mongol Content. All rights reserved.
//

import UIKit

/// Banner slider view in LoginViewController.
class MCGalleryView: UIView, UIScrollViewDelegate {
    
    /// Scroll view in slider.
    private var scrollView: UIScrollView = UIScrollView.init()
    
    /// Page indicator for banner in slider.
    private var pager: UIPageControl = UIPageControl.init()
    
    /// View for a banner in slider.
    private var views: [MCGalleryDetailView] = []
    
    let pageControlHeight = 20
    var isHome = false
    let backgroundImage = UIImageView()
    var tappedImage: ((Int) -> Void)?
    var detailViews = [MCGalleryDetailView]()
    
    
    /// URLs of image array that is shown as banner.
    public var urls: [String] = []{
        didSet {
            pager.numberOfPages = urls.count
            for subview in detailViews {
                subview.removeFromSuperview()
            }
            detailViews.removeAll()
            
            var index = 0
            for urlPath in urls {
                var view: MCGalleryDetailView!
                if isHome {
                    view = HomeGalleryDetailView.init(frame: CGRect.zero, url: urlPath)
                }else{
                    view = MCGalleryDetailView.init(frame: CGRect.zero, url: urlPath)
                }
                detailViews.append(view)
                scrollView.addSubview(view)
                views.append(view)
                view.tag = index
                view.loadImage()
                index += 1
                view.tapHandler = { gellery in
                    self.tapped(sender: gellery)
                }
            }
            setNeedsLayout()
        }
    }
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialize()
    }
    
    func tapped(sender: UIView){
        if let handler = self.tappedImage {
            handler(sender.tag)
        }
    }
    
    
    func initialize() {
        
        backgroundImage.image = UIImage(named: "banner.jpg")
        backgroundImage.contentMode = .scaleAspectFill
        addSubview(backgroundImage)
        
        scrollView.isScrollEnabled = true
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
//        scrollView.backgroundColor = UIColor.white
        scrollView.bounces = false
        scrollView.delegate = self
        addSubview(scrollView)
    
        pager.currentPage = 0
        pager.pageIndicatorTintColor = UIColor.white
        pager.currentPageIndicatorTintColor = Constants.Color.mainGreen
        addSubview(pager)
        Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(onTimer), userInfo: nil, repeats: true)
    }
    
    /// Function which is called by timer every 10 seconds. It scrolls the slider.
    func onTimer() {
        self.pager.currentPage = (self.pager.currentPage == self.pager.numberOfPages - 1) ? 0 : 1 + self.pager.currentPage
        self.scrollView.scrollRectToVisible(
            CGRect.init(
                origin: CGPoint.init(
                    x: CGFloat(self.pager.currentPage) * self.scrollView.bounds.size.width,
                    y: 0),
                size: self.scrollView.bounds.size),
            animated: true)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundImage.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        
        scrollView.frame = CGRect(origin: CGPoint.zero, size: frame.size)
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(urls.count), height: scrollView.frame.size.height)
        pager.frame = CGRect(x: 0, y: scrollView.frame.size.height - CGFloat(pageControlHeight), width: scrollView.frame.size.width, height: CGFloat(pageControlHeight))
        
        for (count, view) in views.enumerated() {
            view.frame = CGRect(origin: CGPoint(x: CGFloat(count) * scrollView.frame.size.width, y: 0), size: scrollView.frame.size)
        }
        
    }
    
    // MARK: - 
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pager.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentOffset.y = 0.0
    }

}
