//
//  Constants.swift
//  hospital
//
//  Created by Ankhbayar TS on 8/24/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

struct Constants {
    
    struct Domain {
        static let domainDev = "http://emneleg.mn/api/"
        static let domainProd = "http://emneleg.mn/api/"
        static let resourceDomain = "http://emneleg.mn/resources/"
        static let webDomain = "http://emneleg.mn/"
        static let domain = domainDev
    }
    
    struct URL {
        static let typeList = Domain.domain + "medical/medical-type"
        static let hospitalList = Domain.domain + "medical/list/"
        static let hospitalDetail = Domain.domain +  "medical/detail/"
        static let adviceList = Domain.domain + "content/list/"
        static let adviceDetail = Domain.domain + "content/detail/"
        static let nurseType = Domain.domain + "nurse/service-type"
        static let nursePosition = Domain.domain + "nurse/position"
        static let nurseList = Domain.domain + "nurse"
        static let diagnoseTestList = Domain.domain + "online-doctor/position/"
        static let diagnoseTestDetail = Domain.domain + "online-doctor/answers/"
    }
    
    struct Color {
        static let mainGreen = UIColor(red: 49/255, green: 189/255, blue: 180/255, alpha: 1)
        static let mainOrange = UIColor(red: 242/255, green: 123/255, blue: 42/255, alpha: 1)
        
    }
    struct String {
        static let mainFont = UIFont.systemFont(ofSize: 16)
    }
    struct CellId {
        static let hospitalList = "HospitalList"
        static let typeCellId = "HomeTypeCell"
    }
    
}
