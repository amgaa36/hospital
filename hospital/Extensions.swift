//
//  Extensions.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/23/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

extension UIView{
    
    func addShadow(){
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 2, height: 13)
        self.layer.shadowRadius = 25
        self.layer.shadowOpacity = 7/255
    }
}

extension String {
    func removingWhitespaces() -> String {
        let components = self.components(separatedBy: .whitespaces).filter{$0 != ""}
        return components.joined(separator: " ")
    }
}
