//
//  HospitalTableDataSource.swift
//  hospital
//
//  Created by user04 on 8/29/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit

class HospitalTableDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {

    var dataArray = [Hospital]()
    var controller: UIViewController!
    var reachedBottom: (()->Void)?
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellId.hospitalList) as! HospitalCell
        let data = dataArray[indexPath.row]
        cell.setData(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 150
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailController = DetailController()
        let data = dataArray[indexPath.row]
        detailController.title = data.name
        detailController.hospitalId = Int(data.id)!
        controller.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if dataArray.count == indexPath.row + 1 {
            if let reachedHandler = self.reachedBottom {
                reachedHandler()
            }
        }
    }
    
}
