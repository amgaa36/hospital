//
//  SuvilagchController.swift
//  hospital
//
//  Created by Ankhbayar TS on 11/18/17.
//  Copyright © 2017 Amgaa. All rights reserved.
//

import UIKit
import RealmSwift
import Presentr
import SwiftyJSON
import SVProgressHUD

class SuvilagchController: HViewController {

    let kCloseCellHeight: CGFloat = 83
    let kOpenCellHeight: CGFloat = 300
    let tableView = UITableView()
    var cellHeights = [CGFloat]()
    let positionDropDown = MKDropdownMenu()
    let serviceDropDown = MKDropdownMenu()
    var nurses: [Nurse] = []
    var positions: [NursePosition] = []
    var services: [NurseServiceType] = []
    var positionIndex = 0
    var serviceIndex = 0
    var json: JSON?
    let presenter = Presentr(presentationType: .popup)
    let refresher = UIRefreshControl()
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        title = "Сувилагч"
        
        let realm = try! Realm()
        positions = Array(realm.objects(NursePosition.self))
        services = Array(realm.objects(NurseServiceType.self))
    
        presenter.cornerRadius = 6
        presenter.roundCorners = true
        presenter.keyboardTranslationType = .stickToTop
        
        positionDropDown.delegate = self
        positionDropDown.dataSource = self
        positionDropDown.backgroundColor = .white
        positionDropDown.tintColor = Constants.Color.mainGreen
        
        view.addSubview(positionDropDown)
        
        tableView.register(SuvilagchCell.self, forCellReuseIdentifier: "SuvilagchCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(white: 0.98, alpha: 1)
        view.addSubview(tableView)
        
        refresher.addTarget(self, action: #selector(self.getData), for: .valueChanged)
        tableView.addSubview(refresher)
        
        positionDropDown.snp.makeConstraints { (const) in
            const.left.top.equalToSuperview()
            const.width.equalToSuperview()
            const.height.equalTo(44)
        }
    
        
        tableView.snp.makeConstraints { (const) in
            const.edges.equalToSuperview().inset(UIEdgeInsetsMake(44, 0, 0, 0))
        }
        
        
        self.getData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData(){
        SVProgressHUD.show()
        let api = APIProvider()
        api.getSuvilagchList(type: services[serviceIndex].id, position: positions[positionIndex].id, success: { (json) in
            SVProgressHUD.dismiss()
            self.refresher.endRefreshing()
            var nurseList = [Nurse]()
            let nurseJsonArray = json["data"].arrayValue
            for nurseJson in nurseJsonArray {
                let nurse = Nurse(json: nurseJson)
                nurseList.append(nurse)
            }
            self.json = json
            self.nurses = nurseList
            self.cellHeights = self.nurses.map{_ in self.kCloseCellHeight}
            self.tableView.reloadData()
        }) { (errorString) in
            SVProgressHUD.dismiss()
            self.refresher.endRefreshing()
            print(errorString)
        }
        
        
    }
    

}

extension SuvilagchController: MKDropdownMenuDataSource, MKDropdownMenuDelegate {
    
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
        return 2
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        return component == 0 ? positions.count : services.count
    }
    
    
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForComponent component: Int) -> String? {
        return component == 0 ? positions[positionIndex].name : services[serviceIndex].name
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, attributedTitleForComponent component: Int) -> NSAttributedString? {
        let attributes = [NSFontAttributeName: UIFont(name: "Roboto", size: 13)!]
        return NSAttributedString(string: component == 0 ? positions[positionIndex].name : services[serviceIndex].name, attributes: attributes)
    }
    
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return positions[row].name
        }
        return services[row].name
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, attributedTitleForSelectedComponent component: Int) -> NSAttributedString? {
        let attributes = [NSFontAttributeName: UIFont(name: "Roboto-Bold", size: 13)!]
        return NSAttributedString(string: component == 0 ? positions[positionIndex].name : services[serviceIndex].name, attributes: attributes)
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributes = [NSFontAttributeName: UIFont(name: "Roboto", size: 13)!]
        if component == 0 {
            return NSAttributedString(string: positions[row].name, attributes: attributes)
        }
        return NSAttributedString(string: services[row].name, attributes: attributes)
    }
    
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, backgroundColorForHighlightedRowsInComponent component: Int) -> UIColor? {
        return UIColor(white: 0.97, alpha: 1)
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, accessoryViewForRow row: Int, forComponent component: Int) -> UIView? {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "ic_check"))
        if component == 0 {
            if row == positionIndex {
                return imageView
            }
        }else if component == 1 {
            if row == serviceIndex {
                return imageView
            }
        }
        return nil
        
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
        
            if component == 0 && positionIndex != row {
                positionIndex = row
                dropdownMenu.closeAllComponents(animated: true)
                dropdownMenu.reloadComponent(component)
                self.getData()
            }else if component == 1 && serviceIndex != row {
                serviceIndex = row
                dropdownMenu.closeAllComponents(animated: true)
                dropdownMenu.reloadComponent(component)
                self.getData()
            }else{
                dropdownMenu.closeAllComponents(animated: true)
            }
    }
    
    
}

extension SuvilagchController:  UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nurses.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as SuvilagchCell = cell else {
            return
        }
        
        cell.backgroundColor = .clear
        
        if cellHeights[indexPath.row] == kCloseCellHeight {
            cell.selectedAnimation(false, animated: false, completion:nil)
        } else {
            cell.selectedAnimation(true, animated: false, completion: nil)
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuvilagchCell") as! SuvilagchCell
        let nurse = nurses[indexPath.row]
        cell.containerImage.kf.setImage(with: URL(string: nurse.nursePhoto), placeholder: #imageLiteral(resourceName: "nurse-list"))
        cell.suvilagchImage.kf.setImage(with: URL(string: nurse.nursePhoto), placeholder: #imageLiteral(resourceName: "nurse-list"))
        cell.containerName.text = nurse.nurseName
        cell.nameLabel.text = nurse.nurseName
        cell.chiglelLabel.setString(key: "Мэргэшсэн чиглэл", value: nurse.nurseMergeshil)
        cell.turshilgaLabel.setString(key: "Ажлын туршилга", value: nurse.nurseTurshlaga)
        cell.namtarLabel.setString(key: "Намтар", value: nurse.nurseNamtar)
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        
        cell.submitAction = {
            
            let api = APIProvider()
            api.sendSuvilagchLog(positionId: self.positions[self.positionIndex].id, nurse: nurse, success: { (json) in
                if json["status"].stringValue.lowercased() == "ok" {
                    if let json = self.json {
                        let resultController = ResultController()
                        resultController.headerLabel.text = json["Header"].stringValue
                        resultController.infoLabel.text = json["Info"].stringValue.replacingOccurrences(of: "\\n", with: "").removingWhitespaces()
                        resultController.bodyLabel.text = json["Body"].stringValue
                        resultController.phoneLabel.text = json["Phone"].stringValue
                        resultController.footerLabel.text = json["Footer"].stringValue
                        self.customPresentViewController(self.presenter, viewController: resultController, animated: true, completion: nil)
                    }
                }
            }, error: { (errorString) in
                print(errorString)
            })
        }
        
        cell.reviewAction = {
            let reviewController = FeedbackController()
            reviewController.nurse = nurse
            self.customPresentViewController(self.presenter, viewController: reviewController, animated: true, completion: nil)
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! SuvilagchCell
        
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == kCloseCellHeight
        if cellIsCollapsed {
            cellHeights[indexPath.row] = kOpenCellHeight
            cell.selectedAnimation(true, animated: true, completion: nil)
            duration = 0.5
        } else {
            cellHeights[indexPath.row] = kCloseCellHeight
            cell.selectedAnimation(false, animated: true, completion: nil)
            duration = 0.8
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
        
    }
    
    
}
